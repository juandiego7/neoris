﻿using PersonClient.Application.DTOs;
using AutoMapper;
using PersonClient.Domain.Entities;

namespace PersonClient.Application.Mappings
{
	public class GeneralProfile : Profile
	{
		public GeneralProfile()
		{
			CreateMap<Client, ClientDto>().ReverseMap();
        }
	}
}

