﻿using PersonClient.Domain.Common;

namespace PersonClient.Application.DTOs
{
	public class ClientDto : Person
	{
        public string Password { get; set; }
        public string Status { get; set; }
    }
}

