﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PersonClient.Application.DTOs;
using PersonClient.Application.Wrappers;

namespace PersonClient.Application.Interfaces.Services
{
	public interface IClientService
	{
        Task<Response<ClientDto>> Create(ClientDto client);
        Task<Response<bool>> Update(ClientDto client);
        Task<Response<bool>> Delete(int id);
        Task<Response<ClientDto>> GetById(int id);
        Task<Response<IEnumerable<ClientDto>>> Get();
    }
}

