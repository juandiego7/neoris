﻿using PersonClient.Domain.Entities;

namespace PersonClient.Application.Interfaces.Repositories
{
    public interface IClientRepository : IRepository<Client>
    {
    }
}

