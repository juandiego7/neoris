﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using AccountTransaction.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountTransaction.Persistence.Configuration
{
    public class TransactionConfig : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.ToTable("Transactions");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Type)
                   .HasMaxLength(5)
                   .IsRequired();

            builder.Property(p => p.Value)
                   .IsRequired();

            builder.Property(p => p.Balance)
                   .IsRequired();

            builder.Property(p => p.AccountId)
                   .IsRequired();

            builder.Property(p => p.Date)
                   .IsRequired();
                    
        }
    }
}



