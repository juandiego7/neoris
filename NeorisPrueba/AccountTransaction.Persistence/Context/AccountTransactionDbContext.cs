﻿using System.Threading.Tasks;
using AccountTransaction.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace AccountTransaction.Persistence.Context
{
    public class AccountTransactionDbContext : DbContext
    {
        private readonly IConfiguration Config;

        public AccountTransactionDbContext(DbContextOptions<AccountTransactionDbContext> options, IConfiguration config) : base(options)
        {
            Config = config;
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public async Task CommitAsync()
        {
            await SaveChangesAsync().ConfigureAwait(false);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(Config.GetSection("SchemaName").Value);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AccountTransactionDbContext).Assembly);
        }
    }
}

