﻿using System;
using AccountTransaction.Application.Interfaces.Repositories;
using AccountTransaction.Domain.Entities;
using System.Collections.Generic;
using AccountTransaction.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace AccountTransaction.Persistence.Repositories
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        private readonly AccountTransactionDbContext _dbContext;
        public TransactionRepository(AccountTransactionDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public int GetInitialBalance(int accountId)
        {
            var account = _dbContext.Set<Account>()
                                    .AsNoTracking()
                                    .AsQueryable()
                                    .FirstOrDefault(a => a.Id == accountId);

            return account.InitialBalance;
        }

        public IEnumerable<Transaction> GetReport(DateTime startDate, DateTime endDate, int clientId)
        {
            return _dbContext.Transactions
                             .Include(t => t.Account).ThenInclude(a => a.Client)
                             .Where(t => t.Date >= startDate && t.Date <= endDate && t.Account.ClientId == clientId);
        }
    }
}

