﻿using AccountTransaction.Application.Interfaces.Repositories;
using AccountTransaction.Domain.Entities;
using AccountTransaction.Persistence.Context;

namespace AccountTransaction.Persistence.Repositories
{
	public class ClientRepository : Repository<Client>, IClientRepository
	{
        public ClientRepository(AccountTransactionDbContext dbContext) : base(dbContext)
        {
        }
    }
}

