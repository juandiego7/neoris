﻿using AccountTransaction.Application.Interfaces.Repositories;
using Ardalis.Specification.EntityFrameworkCore;
using AccountTransaction.Persistence.Context;

namespace AccountTransaction.Persistence.Repositories
{
    public class Repository<T> : RepositoryBase<T>, IRepository<T> where T : class
    {
        public Repository(AccountTransactionDbContext dbContext) : base(dbContext)
        {
        }
    }
}

