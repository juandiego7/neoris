﻿using AccountTransaction.Application.Interfaces.Repositories;
using AccountTransaction.Domain.Entities;
using AccountTransaction.Persistence.Context;

namespace AccountTransaction.Persistence.Repositories
{
    public class AccountRepository : Repository<Account>, IAccountRepository
    {
        public AccountRepository(AccountTransactionDbContext dbContext) : base(dbContext)
        {
        }
    }
}

