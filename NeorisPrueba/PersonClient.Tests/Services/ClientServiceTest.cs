﻿using AutoMapper;
using Moq;
using Xunit;
using PersonClient.Domain.Entities;
using PersonClient.Application.DTOs;
using PersonClient.Application.Interfaces.Repositories;
using PersonClient.Services;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using PersonClient.Application.Exceptions;
using PersonClient.Application.Mappings;

namespace PersonClient.Tests.Services
{
    public class ClientServiceTest
    {
        IMapper mapper;

        public ClientServiceTest()
        {
            mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<GeneralProfile>()));
        }

        [Fact]
        public async Task CreateClientSuccess()
        {
            var clientRepositoryMock = new Mock<IClientRepository>();
            var clientService = new ClientService(clientRepositoryMock.Object, mapper);

            var clientDto = new ClientDto
            {
                Name = "Jose Lema",
                Address = "Otavalo sn y principal",
                Telephone = "098254785",
                Password = "1234",
                Status = "True"
            };

            var clientEntity = new Client
            {
                Id = 1,
                Name = "Jose Lema",
                Address = "Otavalo sn y principal",
                Telephone = "098254785",
                Password = clientService.EncryptPassword(clientDto.Password),
                Status = "True"
            };

            clientRepositoryMock.Setup(repo => repo.AddAsync(It.IsAny<Client>(), It.IsAny<CancellationToken>())).ReturnsAsync(clientEntity);

            var result = await clientService.Create(clientDto);

            Assert.NotNull(result);
            Assert.True(result.IsSuccess);
            Assert.NotNull(result.Result);
            Assert.Equal(clientEntity.Id, result.Result.Id);
            Assert.Equal(clientEntity.Password, result.Result.Password);
            Assert.Equal(clientDto.Name, result.Result.Name);

        }

        [Fact]
        public async Task DeleteClientSuccess()
        {
            var clientRepositoryMock = new Mock<IClientRepository>();
            var clientService = new ClientService(clientRepositoryMock.Object, mapper);

            int clientId = 1;

            var clientToDelete = new Client
            {
                Id = clientId,
                Name = "Jose Lema",
                Address = "Otavalo sn y principal",
                Telephone = "098254785",
                Password = "1234",
                Status = "True"
            };

            clientRepositoryMock.Setup(repo => repo.GetByIdAsync(clientId, It.IsAny<CancellationToken>())).ReturnsAsync(clientToDelete);
            clientRepositoryMock.Setup(repo => repo.DeleteAsync(clientToDelete, It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            var result = await clientService.Delete(clientId);

            Assert.NotNull(result);
            Assert.True(result.IsSuccess);
            Assert.True(result.Result);
        }

        [Fact]
        public async Task GetClients()
        {
            var clientRepositoryMock = new Mock<IClientRepository>();
            var clientService = new ClientService(clientRepositoryMock.Object, mapper);

            var clientEntities = new List<Client>
            {
                 new Client {
                    Id = 1,
                    Name = "Jose Lema",
                    Address = "Otavalo sn y principal",
                    Telephone = "098254785",
                    Password = "1234",
                    Status = "True"
                },
                new Client {
                    Id = 2,
                    Name = "Marianela Montalvo",
                    Address = "Amazonas y NNUU",
                    Telephone = "097548965",
                    Password = "1234",
                    Status = "True"
                }
            };

            var clientDtos = new List<ClientDto>
            {
                new ClientDto {
                    Id = 1,
                    Name = "Jose Lema",
                    Address = "Otavalo sn y principal",
                    Telephone = "098254785",
                    Password = "1234",
                    Status = "True"
                },
                new ClientDto {
                    Id = 2,
                    Name = "Marianela Montalvo",
                    Address = "Amazonas y NNUU",
                    Telephone = "097548965",
                    Password = "1234",
                    Status = "True"
                }
            };

            clientRepositoryMock.Setup(repo => repo.ListAsync(It.IsAny<CancellationToken>())).ReturnsAsync(clientEntities);

            var result = await clientService.Get();

            Assert.NotNull(result);
            Assert.True(result.IsSuccess);
            Assert.NotNull(result.Result);
            Assert.Equal(clientEntities.Count, result.Result.Count());
        }

        [Fact]
        public async Task GetClientByIdSuccess()
        {
            var clientRepositoryMock = new Mock<IClientRepository>();
            var clientService = new ClientService(clientRepositoryMock.Object, mapper);

            int clientId = 1; 

            var clientEntity = new Client
            {
                Id = clientId,
                Name = "Jose Lema",
                Address = "Otavalo sn y principal",
                Telephone = "098254785",
                Password = "1234",
                Status = "True"
            };

            var clientDto = new ClientDto
            {
                Id = clientId,
                Name = "Jose Lema",
                Address = "Otavalo sn y principal",
                Telephone = "098254785",
                Password = "1234",
                Status = "True"
            };

            clientRepositoryMock.Setup(repo => repo.GetByIdAsync(clientId, It.IsAny<CancellationToken>())).ReturnsAsync(clientEntity);

            var result = await clientService.GetById(clientId);

            Assert.NotNull(result);
            Assert.True(result.IsSuccess);
            Assert.NotNull(result.Result);
            Assert.Equal(clientDto.Id, result.Result.Id);
        }

        [Fact]
        public async Task GetClientByIdErrorClientNotFoundException()
        {
            var clientRepositoryMock = new Mock<IClientRepository>();
            var clientService = new ClientService(clientRepositoryMock.Object, mapper);

            int clientId = 1;

            clientRepositoryMock.Setup(repo => repo.GetByIdAsync(clientId, It.IsAny<CancellationToken>())).ReturnsAsync((Client)null);

            await Assert.ThrowsAsync<ClientNotFoundException>(() => clientService.GetById(clientId));
        }

    }
}

