﻿using System.Threading.Tasks;
using AccountTransaction.Application.DTOs;
using AccountTransaction.Application.Exceptions;
using AccountTransaction.Application.Interfaces.Services;
using MassTransit;
using PersonClient.Contracts.Transactions;

namespace AccountTransaction.Services.Consumers
{
    public class ClientUpdatedConsumer : IConsumer<ClientUpdated>
    {
        private readonly IClientService _service;

        public ClientUpdatedConsumer(IClientService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<ClientUpdated> context)
        {
            var message = context.Message;

            try
            {
                var response = await _service.GetById(message.Id);
                if (response.IsSuccess)
                {
                    var client = response.Result;

                    client.Id = message.Id;
                    client.Name = message.Name;
                    client.Gender = message.Gender;
                    client.Age = message.Age;
                    client.PersonId = message.PersonId;
                    client.Address = message.Address;
                    client.Telephone = message.Telephone;
                    client.Status = message.Status;                    

                    await _service.Update(client);
                }
                
            }
            catch (ClientNotFoundException)
            {
                var newClient = new ClientDto
                {
                    Id = message.Id,
                    Name = message.Name,
                    Gender = message.Gender,
                    Age = message.Age,
                    PersonId = message.PersonId,
                    Address = message.Address,
                    Telephone = message.Telephone,
                    Status = message.Status
                };

                await _service.Create(newClient);
            }                                    
        }
    }
}

