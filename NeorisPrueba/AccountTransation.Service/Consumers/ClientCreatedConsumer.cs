﻿using System.Threading.Tasks;
using AccountTransaction.Application.DTOs;
using AccountTransaction.Application.Interfaces.Services;
using MassTransit;
using PersonClient.Contracts.Transactions;

namespace AccountTransaction.Service.Consumers
{
    public class ClientCreatedConsumer : IConsumer<ClientCreated>
    {
        private readonly IClientService _service;

        public ClientCreatedConsumer(IClientService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<ClientCreated> context)
        {
            var message = context.Message;
            var response = await _service.Exists(message.Id);
            if (response.IsSuccess && !response.Result)
            {
                var newClient = new ClientDto
                {
                    Id = message.Id,
                    Name = message.Name,
                    Gender = message.Gender,
                    Age = message.Age,
                    PersonId = message.PersonId,
                    Address = message.Address,
                    Telephone = message.Telephone,
                    Status = message.Status
                };
                await _service.Create(newClient);
            }
        }
    }
}

