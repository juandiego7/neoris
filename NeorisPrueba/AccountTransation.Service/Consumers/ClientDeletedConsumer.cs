﻿using System.Threading.Tasks;
using AccountTransaction.Application.Interfaces.Services;
using MassTransit;
using PersonClient.Contracts.Transactions;

namespace AccountTransaction.Services.Consumers
{
    public class ClientDeletedConsumer : IConsumer<ClientDeleted>
    {
        private readonly IClientService _service;

        public ClientDeletedConsumer(IClientService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<ClientDeleted> context)
        {
            var message = context.Message;
            
            var response = await _service.Exists(message.Id);

            if (response.IsSuccess && response.Result)
            {
                await _service.Delete(message.Id);
            }
        }
    }
}

