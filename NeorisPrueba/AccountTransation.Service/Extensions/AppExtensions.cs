﻿using System;
using AccountTransation.Service.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace AccountTransation.Service.Extensions
{
    public static class AppExtensions
    {
        public static void UseErrorHandlingMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlerMiddleware>();
        }
    }
}

