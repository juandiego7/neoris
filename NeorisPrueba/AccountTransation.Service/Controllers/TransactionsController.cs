using System.Collections.Generic;
using System.Threading.Tasks;
using AccountTransaction.Application.DTOs;
using AccountTransaction.Application.Interfaces.Services;
using AccountTransaction.Application.Wrappers;
using Microsoft.AspNetCore.Mvc;

namespace AccountTransation.Service.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/movimientos")]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionService _service;
        public TransactionsController(ITransactionService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public async Task<Response<TransactionDto>> GetAsync(int id)
        {
            return await _service.GetById(id);
        }

        [HttpGet]
        public async Task<Response<IEnumerable<TransactionDto>>> Get()
        {
            return await _service.Get();
        }

        [HttpPost]
        public async Task<Response<TransactionDto>> Post(TransactionDto transaction)
        {
            return await _service.Create(transaction);
        }

        [HttpPut]
        public async Task<Response<bool>> Put(TransactionDto transaction)
        {
            return await _service.Update(transaction);
        }

        [HttpDelete("{id}")]
        public async Task<Response<bool>> Delete(int id)
        {
            return await _service.Delete(id);
        }

        [HttpGet("reportes/{startDate}/{endDate}/{clientId}")]
        public Response<IEnumerable<ReportDto>> GetReport(string startDate, string endDate, int clientId)
        {
            return _service.GetReport(startDate, endDate, clientId);
        }
    }
}
