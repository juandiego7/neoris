using System.Collections.Generic;
using System.Threading.Tasks;
using AccountTransaction.Application.DTOs;
using AccountTransaction.Application.Interfaces.Services;
using AccountTransaction.Application.Wrappers;
using Microsoft.AspNetCore.Mvc;

namespace AccountTransation.Service.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/cuentas")]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService _service;
        public AccountsController(IAccountService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public async Task<Response<AccountDto>> GetAsync(int id)
        {
            return await _service.GetById(id);
        }

        [HttpGet]
        public async Task<Response<IEnumerable<AccountDto>>> Get()
        {
            return await _service.Get();
        }

        [HttpPost]
        public async Task<Response<AccountDto>> Post(AccountDto account)
        {
            return await _service.Create(account);
        }

        [HttpPut]
        public async Task<Response<bool>> Put(AccountDto account)
        {
            return await _service.Update(account);
        }

        [HttpDelete("{id}")]
        public async Task<Response<bool>> Delete(int id)
        {
            return await _service.Delete(id);
        }
    }
}
