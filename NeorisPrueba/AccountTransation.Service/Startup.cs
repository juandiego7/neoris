﻿using AccountTransation.Service.Extensions;
using AccountTransaction.Application;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using AccountTransaction.Persistence;
using AccountTransaction.Persistence.Context;
using AccountTransaction.Services;
using Common.MassTransit;
namespace AccountTransation.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationLayer();
            services.AddPersistenceInfraestructure(Configuration);
            services.AddDbContext<AccountTransactionDbContext>(opt =>
            {
                opt.UseInMemoryDatabase("AccountTransaction.Service");
            });
            //services.AddDbContext<AccountTransactionDbContext>(c =>
            //{
            //    c.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
            //                   b => b.MigrationsAssembly("AccountTransaction.Persistence"));
            //});
            services.AddServicesCore();
            services.AddMassTransitWithRabbitMq();
            services.AddApiVersioningExtension();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AccountTransaction.Service", Version = "v1" });
            });
            services.AddControllers();
            services.AddControllersWithViews()
                .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AccountTransaction.Service v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseErrorHandlingMiddleware();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

