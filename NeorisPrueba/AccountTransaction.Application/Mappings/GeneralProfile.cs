﻿using AccountTransaction.Application.DTOs;
using AutoMapper;
using AccountTransaction.Domain.Entities;

namespace AccountTransaction.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<Client, ClientDto>().ReverseMap();
            CreateMap<Account, AccountDto>().ReverseMap();
            CreateMap<Transaction, TransactionDto>().ReverseMap();
        }
    }
}

