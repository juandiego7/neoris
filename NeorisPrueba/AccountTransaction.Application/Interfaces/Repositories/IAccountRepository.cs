﻿using AccountTransaction.Domain.Entities;

namespace AccountTransaction.Application.Interfaces.Repositories
{
    public interface IAccountRepository : IRepository<Account>
    {
    }
}

