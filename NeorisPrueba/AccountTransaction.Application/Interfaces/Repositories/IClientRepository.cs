﻿using AccountTransaction.Domain.Entities;

namespace AccountTransaction.Application.Interfaces.Repositories
{
	public interface IClientRepository : IRepository<Client>
	{
	}
}

