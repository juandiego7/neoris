﻿using Ardalis.Specification;

namespace AccountTransaction.Application.Interfaces.Repositories
{
    public interface IRepository<T> : IRepositoryBase<T>, IReadRepositoryBase<T> where T : class
    {
    }
}

