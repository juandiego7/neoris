﻿using System;
using System.Collections.Generic;
using AccountTransaction.Domain.Entities;

namespace AccountTransaction.Application.Interfaces.Repositories
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
        public int GetInitialBalance(int accountId);

        public IEnumerable<Transaction> GetReport(DateTime startDate, DateTime endDate, int clientId);
    }
}

