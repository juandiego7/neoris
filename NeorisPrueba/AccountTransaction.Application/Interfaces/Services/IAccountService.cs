﻿using AccountTransaction.Application.DTOs;
using AccountTransaction.Application.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AccountTransaction.Application.Interfaces.Services
{
    public interface IAccountService
    {
        Task<Response<AccountDto>> Create(AccountDto account);
        Task<Response<bool>> Update(AccountDto account);
        Task<Response<bool>> Delete(int id);
        Task<Response<AccountDto>> GetById(int id);
        Task<Response<IEnumerable<AccountDto>>> Get();
    }
}

