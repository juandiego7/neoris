﻿using AccountTransaction.Application.DTOs;
using AccountTransaction.Application.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AccountTransaction.Application.Interfaces.Services
{
    public interface ITransactionService
    {
        Task<Response<TransactionDto>> Create(TransactionDto transaction);
        Task<Response<bool>> Update(TransactionDto transaction);
        Task<Response<bool>> Delete(int id);
        Task<Response<TransactionDto>> GetById(int id);
        Task<Response<IEnumerable<TransactionDto>>> Get();
        Response<IEnumerable<ReportDto>> GetReport(string startDate, string endDate, int clientId);
    }
}

