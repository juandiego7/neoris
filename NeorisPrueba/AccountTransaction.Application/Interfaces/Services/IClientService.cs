﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AccountTransaction.Application.DTOs;
using AccountTransaction.Application.Wrappers;

namespace AccountTransaction.Application.Interfaces.Services
{
	public interface IClientService
	{
        Task<Response<ClientDto>> Create(ClientDto client);
        Task<Response<bool>> Update(ClientDto client);
        Task<Response<bool>> Delete(int id);
        Task<Response<ClientDto>> GetById(int id);
        Task<Response<bool>> Exists(int id);
    }
}

