﻿namespace AccountTransaction.Application.Exceptions
{
	public class ClientNotFoundException : NotFoundException
	{
		public ClientNotFoundException(string id) : base ($"El ciente con id {id} no fue encontrado.")
		{
		}
	}
}

