﻿using System;

namespace AccountTransaction.Application.DTOs
{
	public class TransactionDto
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public int Value { get; set; }
        public int Balance { get; set; }
        public int AccountId { get; set; }
        public AccountDto Account { get; set; }
    }
}

