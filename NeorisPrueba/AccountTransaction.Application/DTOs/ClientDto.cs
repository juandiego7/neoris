﻿using System.Collections.Generic;
using AccountTransaction.Domain.Common;

namespace AccountTransaction.Application.DTOs
{
    public class ClientDto : Person
    {
        public string Status { get; set; }
        public ICollection<AccountDto> Accounts { get; set; }
    }
}

