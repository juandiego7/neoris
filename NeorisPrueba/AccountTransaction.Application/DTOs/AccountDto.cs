﻿namespace AccountTransaction.Application.DTOs
{
	public class AccountDto
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Type { get; set; }
        public int InitialBalance { get; set; }
        public string Status { get; set; }
        public int ClientId { get; set; }
        public ClientDto Client { get; set; }
    }
}

