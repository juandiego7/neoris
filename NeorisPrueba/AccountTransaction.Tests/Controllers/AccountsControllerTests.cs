﻿using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using Xunit;
using FluentAssertions;
using AccountTransation.Service;
using AccountTransaction.Application.Wrappers;
using AccountTransaction.Application.DTOs;

namespace AccountTransation.Tests.Controllers
{
	public class AccountsControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public AccountsControllerTests(WebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public void PostAccountSuccess()
        {
            var newAccount = new
            {
                ClientId = 1,
                Number = "478758",
                Type = "Ahorro",
                InitialBalance = 2000,
                Status = "True"
            };

            var content = new StringContent(JsonConvert.SerializeObject(newAccount), Encoding.UTF8, "application/json");
            var resultPost = this._client.PostAsync("/api/v1/cuentas", content).Result;
            resultPost.EnsureSuccessStatusCode();
            var stringContent = resultPost.Content.ReadAsStringAsync().Result;
            var responseAccountPost = JsonConvert.DeserializeObject<Response<AccountDto>>(stringContent);
            var idAccount = responseAccountPost.Result.Id;

            var resultGet = this._client.GetAsync($"api/v1/cuentas/{idAccount}").Result;
            resultGet.EnsureSuccessStatusCode();
            stringContent = resultGet.Content.ReadAsStringAsync().Result;
            var responseGet = JsonConvert.DeserializeObject<Response<AccountDto>>(stringContent);
            var account = responseGet.Result;

            account.ClientId.Should().Be(newAccount.ClientId);
            account.Number.Should().Be(newAccount.Number);
        }
    }
}

