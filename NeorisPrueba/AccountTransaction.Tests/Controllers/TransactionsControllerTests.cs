﻿using System;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using Xunit;
using FluentAssertions;
using System.Net;
using AccountTransation.Service;
using AccountTransaction.Application.Wrappers;
using AccountTransaction.Application.DTOs;

namespace AccountTransation.Tests.Controllers
{
	public class TransactionsControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public TransactionsControllerTests(WebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public void PostTransactionSuccess()
        {           
            var newAccount = new
            {
                ClientId = 1,
                Number = "478758",
                Type = "Ahorro",
                InitialBalance = 2000,
                Status = "True"
            };

            var content = new StringContent(JsonConvert.SerializeObject(newAccount), Encoding.UTF8, "application/json");
            var resultPost = this._client.PostAsync("/api/v1/cuentas", content).Result;
            resultPost.EnsureSuccessStatusCode();
            var stringContent = resultPost.Content.ReadAsStringAsync().Result;
            var responseAccountPost = JsonConvert.DeserializeObject<Response<AccountDto>>(stringContent);
            var idAccount = responseAccountPost.Result.Id;

            var newTransacction = new 
            {
                Value = 1000,
                AccountId = idAccount
            };

            content = new StringContent(JsonConvert.SerializeObject(newTransacction), Encoding.UTF8, "application/json");
            resultPost = this._client.PostAsync("/api/v1/movimientos", content).Result;
            resultPost.EnsureSuccessStatusCode();
            stringContent = resultPost.Content.ReadAsStringAsync().Result;
            var responseTransactionPost = JsonConvert.DeserializeObject<Response<TransactionDto>>(stringContent);
            var idTransaction = responseTransactionPost.Result.Id;

            var resultGet = this._client.GetAsync($"api/v1/movimientos/{idTransaction}").Result;
            resultGet.EnsureSuccessStatusCode();
            stringContent = resultGet.Content.ReadAsStringAsync().Result;
            var responseGet = JsonConvert.DeserializeObject<Response<TransactionDto>>(stringContent);
            var transaction = responseGet.Result;

            transaction.Balance.Should().Be(newAccount.InitialBalance + newTransacction.Value);
            transaction.AccountId.Should().Be(newTransacction.AccountId);
        }

        [Fact]
        public void PostTransactionErrorUnavailableBalance()
        {
            var newAccount = new
            {
                ClientId = 1,
                Number = "478758",
                Type = "Ahorro",
                InitialBalance = 0,
                Status = "True"
            };

            var content = new StringContent(JsonConvert.SerializeObject(newAccount), Encoding.UTF8, "application/json");
            var resultPost = this._client.PostAsync("/api/v1/cuentas", content).Result;
            resultPost.EnsureSuccessStatusCode();
            var stringContent = resultPost.Content.ReadAsStringAsync().Result;
            var responseAccountPost = JsonConvert.DeserializeObject<Response<AccountDto>>(stringContent);
            var idAccount = responseAccountPost.Result.Id;

            var newTransacction = new
            {
                Value = -1000,
                AccountId = idAccount
            };
            try
            {
                content = new StringContent(JsonConvert.SerializeObject(newTransacction), Encoding.UTF8, "application/json");
                resultPost = this._client.PostAsync("/api/v1/movimientos", content).Result;
                resultPost.EnsureSuccessStatusCode();
                Assert.True(false, "Deberia fallar");
            }
            catch (Exception)
            {
                resultPost.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            }                                        
        }
    }
}

