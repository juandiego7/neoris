﻿namespace PersonClient.Contracts.Transactions
{
    public record ClientDeleted(int Id);
}

