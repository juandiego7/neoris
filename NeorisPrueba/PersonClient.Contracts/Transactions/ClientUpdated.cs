﻿namespace PersonClient.Contracts.Transactions
{
	public record ClientUpdated
	(
        int Id,
        string Name,
        string Gender,
        int Age,
        string PersonId,
        string Address,
        string Telephone,
        string Status
    );
}

