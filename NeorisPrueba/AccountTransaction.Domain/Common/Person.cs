﻿namespace AccountTransaction.Domain.Common
{
	public abstract class Person 
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Gender { get; set; }
		public int Age { get; set; }
		public string PersonId { get; set; }
		public string Address { get; set; }
		public string Telephone { get; set; }
	}
}

