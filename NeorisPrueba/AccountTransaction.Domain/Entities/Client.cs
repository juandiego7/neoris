﻿using AccountTransaction.Domain.Common;

namespace AccountTransaction.Domain.Entities
{
	public class Client : Person
	{
		public string Status { get; set; }
    }
}

