﻿namespace AccountTransaction.Domain.Entities
{
    public class Account 
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Type { get; set; }
        public int InitialBalance { get; set; }
        public string Status { get; set; }
        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}



