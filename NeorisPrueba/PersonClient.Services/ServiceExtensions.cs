﻿using PersonClient.Application.Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;

namespace PersonClient.Services
{
    public static class ServiceExtensions
    {
        public static void AddServicesCore(this IServiceCollection services)
        {
            services.AddTransient<IClientService, ClientService>();            
        }
    }
}

