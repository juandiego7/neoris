﻿using System.Threading.Tasks;
using AccountTransaction.Application.DTOs;
using AccountTransaction.Application.Interfaces.Repositories;
using AccountTransaction.Application.Interfaces.Services;
using AccountTransaction.Application.Wrappers;
using AutoMapper;
using AccountTransaction.Domain.Entities;
using AccountTransaction.Application.Exceptions;

namespace AccountTransaction.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _repository;
        private readonly IMapper _mapper;

        public ClientService(IClientRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<Response<ClientDto>> Create(ClientDto client)
        {
            var clientMap = _mapper.Map<Client>(client);
            var result = await _repository.AddAsync(clientMap);
            var clientCreated = _mapper.Map<ClientDto>(result);
            return new Response<ClientDto>(clientCreated);
        }

        public async Task<Response<bool>> Delete(int id)
        {
            var client = await _repository.GetByIdAsync(id);
            await _repository.DeleteAsync(client);
            return new Response<bool>(true);
        }

        public async Task<Response<bool>> Exists(int id)
        {
            var client = await _repository.GetByIdAsync(id);         
            return new Response<bool>(client != null);              
        }

        public async Task<Response<ClientDto>> GetById(int id)
        {
            var result = await _repository.GetByIdAsync(id);
            var client = _mapper.Map<ClientDto>(result);
            if(client is null)
            {
                throw new ClientNotFoundException(id.ToString());
            }
            return new Response<ClientDto>(client);
        }

        public async Task<Response<bool>> Update(ClientDto client)
        {
            var clientMap = _mapper.Map<Client>(client);
            await _repository.UpdateAsync(clientMap);
            return new Response<bool>(true);
        }        
    }
}

