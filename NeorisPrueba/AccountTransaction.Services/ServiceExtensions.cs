﻿using AccountTransaction.Application.Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;

namespace AccountTransaction.Services
{
    public static class ServiceExtensions
    {
        public static void AddServicesCore(this IServiceCollection services)
        {
            services.AddTransient<IClientService, ClientService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<ITransactionService, TransactionService>();
        }
    }
}

