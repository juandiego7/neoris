﻿using System.Reflection;
using Common.Settings;
using MassTransit;
using MassTransit.Definition;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Common.MassTransit
{
	public static class Extensions
	{
		public static IServiceCollection AddMassTransitWithRabbitMq(this IServiceCollection services)
		{
            services.AddMassTransit(configure =>
            {
                configure.AddConsumers(Assembly.GetEntryAssembly());

                configure.UsingRabbitMq((context, configurator) =>
                {
                    var configuration = context.GetService<IConfiguration>();
                    var host = configuration.GetSection(nameof(RabbitMQSettings)).GetSection("Host").Value;
                    var serviceName = configuration.GetSection("ServiceSettings").GetSection("ServiceName").Value;
                    configurator.Host(host);
                    configurator.ConfigureEndpoints(context, new KebabCaseEndpointNameFormatter(serviceName, false));
   
                });
            });

            services.AddMassTransitHostedService();

            return services;
        }
	}
}

