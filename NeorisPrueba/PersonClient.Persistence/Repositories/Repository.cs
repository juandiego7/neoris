﻿using PersonClient.Application.Interfaces.Repositories;
using Ardalis.Specification.EntityFrameworkCore;
using PersonClient.Persistence.Context;

namespace PersonClient.Persistence.Repositories
{
	public class Repository<T> : RepositoryBase<T>, IRepository<T> where T : class
    {
		public Repository(PersonClientDbContext dbContext) : base(dbContext)
		{
		}
	}
}

