﻿using PersonClient.Application.Interfaces.Repositories;
using PersonClient.Domain.Entities;
using PersonClient.Persistence.Context;

namespace PersonClient.Persistence.Repositories
{
	public class ClientRepository : Repository<Client>, IClientRepository
	{
        public ClientRepository(PersonClientDbContext dbContext) : base(dbContext)
        {
        }
    }
}

