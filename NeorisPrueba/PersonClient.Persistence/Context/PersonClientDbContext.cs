﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PersonClient.Domain.Entities;

namespace PersonClient.Persistence.Context
{
	public class PersonClientDbContext : DbContext
	{
        private readonly IConfiguration Config;

        public PersonClientDbContext(DbContextOptions<PersonClientDbContext> options, IConfiguration config) : base(options)
		{
            Config = config;
        }

        public DbSet<Client> Clients { get; set; }

        public async Task CommitAsync()
        {
            await SaveChangesAsync().ConfigureAwait(false);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(Config.GetSection("SchemaName").Value);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PersonClientDbContext).Assembly);
        }
    }
}
