using System.Collections.Generic;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using PersonClient.Application.DTOs;
using PersonClient.Application.Interfaces.Services;
using PersonClient.Contracts.Transactions;

namespace PersonClient.Service.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/clientes")]
    public class ClientsController : ControllerBase
    {
        private readonly IClientService _service;
        private readonly IPublishEndpoint _publishEndpoint;

        public ClientsController(IClientService service, IPublishEndpoint publishEndpoint)
        {
            _service = service;
            _publishEndpoint = publishEndpoint;
        }

        [HttpGet("{id}")]
        public async Task<Application.Wrappers.Response<ClientDto>> Get(int id)
        {            
            return await _service.GetById(id);
        }

        [HttpGet]
        public async Task<Application.Wrappers.Response<IEnumerable<ClientDto>>> Get()
        {
            return await _service.Get();
        }

        [HttpPost]
        public async Task<Application.Wrappers.Response<ClientDto>> Post(ClientDto client)
        {
            var result = await _service.Create(client);
            if (result.IsSuccess)
            {
                var message = new ClientCreated(result.Result.Id, client.Name, client.Gender, client.Age, client.PersonId, client.Address, client.Telephone, client.Status);
                await _publishEndpoint.Publish(message);
            }
            return result;
        }

        [HttpPut]
        public async Task<Application.Wrappers.Response<bool>> Put(ClientDto client)
        {
            var result = await _service.Update(client);
            var message = new ClientUpdated(client.Id, client.Name, client.Gender, client.Age, client.PersonId, client.Address, client.Telephone, client.Status);          
            await _publishEndpoint.Publish(message);   
            return result;
        }

        [HttpDelete("{id}")]
        public async Task<Application.Wrappers.Response<bool>> Delete(int id)
        {
            var result = await _service.Delete(id);            
            await _publishEndpoint.Publish(new ClientDeleted(id));
            return result;
        }
    }
}