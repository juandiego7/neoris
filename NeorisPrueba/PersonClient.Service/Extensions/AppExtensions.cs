﻿using Microsoft.AspNetCore.Builder;
using PersonClient.Middlewares;

namespace PersonClient.Extensions
{
	public static class AppExtensions
	{
		public static void UseErrorHandlingMiddleware(this IApplicationBuilder app)
		{
			app.UseMiddleware<ErrorHandlerMiddleware>();
		}
	}
}

