﻿using PersonClient.Domain.Common;

namespace PersonClient.Domain.Entities
{
	public class Client : Person
	{
		public string Password { get; set; }
		public string Status { get; set; }
    }
}

